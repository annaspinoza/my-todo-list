import { LitElement, css } from 'lit-element';
import { nothing, html } from 'lit-html';
import './input-field.js';
import './my-list.js'; 
import './action-bar.js';
import './my-button.js';

class TodoList extends LitElement {
    static get properties() {
        return {
            todos: { type: Array },
            listName: { type: String },
        };
    }

    static get styles() {
        return css`
            .header {
                display: flex;
                justify-content: center;
                margin: 15px 50px auto;
                height: 50px;
            }

            .container {
                display: flex;
                justify-content: center;
                padding: 0 60px 16px;
            }
            
            my-button {
                position: relative;
                bottom: 8px;
            }
        `;
    }

    constructor(){
        super();
        this.inputPlaceholder = 'Add a todo task';
        this.todos = []; 
        this.buttonName = 'X';
        this.buttonType = {"genericButton":true , delete:true};
    }

    connectedCallback(){
        super.connectedCallback();
        this.todos = JSON.parse(localStorage.getItem(this.listName)) ?? [];
    }

    addNewTodo(e) {
        let todoItem = {
            value: e.detail.value,
            checked: false,
            id: Math.random(),
        }
        this.todos = [...this.todos, todoItem];
    }

    toggleSelected(e) {
        let toggledItem = e.detail.selectedItem;
        toggledItem.checked = !toggledItem.checked;
        this.todos = this.todos.map(item => item.id === toggledItem.id ? toggledItem : item);
    }

    deleteSelected(e) {
        let deletedItem = e.detail.selectedItem;
        this.todos = this.todos.filter(item => item.id != deletedItem.id)
    }

    clearList(e) {
        this.todos = [];
    }

    onRemoveList(){
        let event = new CustomEvent('removeList', {
            detail: {
                value: this.listName,
            }
        });
        this.dispatchEvent(event);
    }

      
    render() {
        return html`
            <header class="header">
                <h1 class="header__title">${this.listName.toUpperCase()}<h1>
                <my-button .buttonType='${this.buttonType}' .buttonName='${this.buttonName}' @onClick="${this.onRemoveList}"></my-button>
            </header>
            
            <div class="container">
                <main class="main">
                    <input-field .placeholder="${this.inputPlaceholder}" @onNewEntry="${this.addNewTodo}"></input-field>
                    <my-list .items="${this.todos}" @onTick="${this.toggleSelected}" @onDelete="${this.deleteSelected}"></my-list>
                    ${this.todos.length > 0
                        ? html`<action-bar @onClear="${this.clearList}"></action-bar>`
                        : nothing
                    }
                </main>
            </div>
        `;
    }

    updated(changedProperties) {
        if(changedProperties.has("todos")) {
            localStorage.setItem(this.listName, JSON.stringify(this.todos));
        }
        if(changedProperties.has("listName")) {
            this.todos = JSON.parse(localStorage.getItem(this.listName)) ?? []
        }
    }
}
customElements.define('todo-list', TodoList);


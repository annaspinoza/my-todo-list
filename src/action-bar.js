import { LitElement, html, css } from 'lit-element';
import './my-button.js';

class ActionBar extends LitElement {
    static get styles(){
        return css`
            .button-row {
                display: flex;
                justify-content: flex-end;
            }
            .clear {
                margin: 30px 10px 10px auto;
            }
        `;
    }

    constructor(){
        super();
        this.buttonName = 'Clear List';
        this.buttonType = {"genericButton":true};
    }

    
    onClear() {
        let event = new CustomEvent('onClear', {
        });
        this.dispatchEvent(event);
    }

    render(){
        return html`
            <span class="button-row">
                <div class="clear"><my-button .buttonType='${this.buttonType}' .buttonName='${this.buttonName}' @onClick="${this.onClear}"></my-button><div>
            </span>
        `;
    }
}
customElements.define('action-bar', ActionBar);

import { LitElement, html, css } from 'lit-element';
import './my-button.js';

class InputField extends LitElement {
    static get properties() {
        return {
            inputEntry: { type: String },
            label: { type: String },
            placeholder: { type: String }, 
        };
    }

    static get styles() {
        return css`
            .input-field__block {
                margin: 0 24px 8px;
                width: 520px;
            }

            .input-field__label {
                display: flex;
                justify-content: center;
                font-size: 24px;
                margin: 10px;
            }

            .input-field__box {
                margin: 8px;
                padding: 2px;
                width: 400px;
                height: 25px;
            }
        `
    }

    constructor() {
        super();
        this.inputEntry = '';
        this.buttonName = 'Add';
    }

    shortcutListener(e) {
        if (e.key === 'Enter') { 
            this.onNewEntry();
        }
    }

    onUpdate(e) {
        this.inputEntry = e.target.value;
    }

    onNewEntry() {
        if (this.inputEntry) {
            let event = new CustomEvent('onNewEntry', {
                detail: {
                    value: this.inputEntry,
                }
              });
            this.dispatchEvent(event);
            this.inputEntry = '';
            this.shadowRoot.getElementById('input').value = "";
        };
    }

    render() {
        return html`       
            <div class="input-field__block" @keyup="${this.shortcutListener}">
                <label class="input-field__label">${this.label} </label>
                <input id="input" type="text" class="input-field__box" placeholder="${this.placeholder}" 
                    maxlength="30" value="${this.inputEntry}" @change="${this.onUpdate}">
                <my-button .buttonName='${this.buttonName}' @onClick="${this.onNewEntry}"></my-button>
            </div>          
        `;
    }
    
}

customElements.define('input-field', InputField);

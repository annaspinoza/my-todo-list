import { LitElement, html, css } from 'lit-element';
import { classMap } from 'lit-html/directives/class-map';

class MyButton extends LitElement {
    static get properties() {
        return {
            buttonName: { type: String },
            buttonType:  { type: Object },
        };
    }
    static get styles(){
        return css`
            .genericButton {
                display: inline-block;
                padding: 5px 15px;
                margin: 15px;
                font-size: 14px;
                font-weight: 700;
                cursor: pointer;
                text-align: center;
                text-decoration: none;
                outline: none;
                color: indigo;
                background-color: orchid;
                border: none;
                border-radius: 15px;
            }

            .delete {
                background-color: lavenderblush;
                font-size: 12px;
            }
            
        `;
    }

    constructor(){
        super();
        this.buttonType = {"genericButton":true};
    }

    
    onClick() {
        let event = new CustomEvent('onClick', {
        });
        this.dispatchEvent(event);
    }

    render(){
        return html`
            <button type="button" class="${classMap(this.buttonType)}" @click="${this.onClick}">${this.buttonName}</button>
        `;
    }
}
customElements.define('my-button', MyButton);

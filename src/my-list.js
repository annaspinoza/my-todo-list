import { LitElement, html, css } from 'lit-element';
import './my-button.js';


class MyList extends LitElement {
    static get properties() {
        return {
            items: { type: Array },
        };
    }

    static get styles() {
        return css`
            .list {
                list-style-type: none;
                list-style-position: inside;
                font-size: 14px;
                padding-inline-start: 0;
            }

            .list__item {
                line-height: 1.5;
                display: block;
                position: relative;
                padding-left: 30px;
                margin-bottom: 8px;
            }

            .checked {
                text-decoration: line-through;
                color: green;
            }

            .checkbox {
                position: absolute;
                left: 0;
                height: 20px;
                width: 20px;
            }

            .deleteItem {
                position: absolute;
                right: 40px;
                top: -13px;
            }
        `;
    }
    
    constructor(){
        super();
        this.buttonName = 'X';
        this.buttonType = {"genericButton":true , delete:true};
    }
    
    onTick(item){
        let event = new CustomEvent('onTick', {
            detail: {
                selectedItem: item,
            }
          });
        this.dispatchEvent(event);
    }

    onDelete(item){
        let event = new CustomEvent('onDelete', {
            detail: {
                selectedItem: item,
            }
          });
        this.dispatchEvent(event);
    }
    
    render(){
        return html`
            <ul class="list">
                ${this.items.map(item => html`
                    <li class="list__item">
                        <input type="checkbox" class="checkbox" id="${item.id}" @click="${() => this.onTick(item)}" ?checked="${item.checked}">
                        <label for="${item.id}" class="${item.checked ? 'checked' : ''}"> ${item.value}</label>
                        <div class="deleteItem"><my-button .buttonType='${this.buttonType}' .buttonName='${this.buttonName}' @onClick="${() => this.onDelete(item)}"></my-button></div>
                    </li>
                `)}
            </ul>
        `;
    }
}

customElements.define('my-list', MyList);

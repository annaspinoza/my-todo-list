import { LitElement, html, css } from 'lit-element';

class SidebarMenu extends LitElement {
    static get properties(){
        return{
            menu: { type: Array },
        };
    }

    static get styles(){
        return css`
            .sidebar__content {
                padding:20px;
                line-height: 3;
                padding: 130px 8px 8px 16px;
                text-decoration: none;
                display: block;
            }
            
            button {
                outline: none;
                background: transparent;
                border: none;
                font-size: 16px;
                color: indigo;
            }
            
            button:hover {
                font-size: 18px;
                font-weight: 700;
            }
        `;
    }

    constructor(){
        super();
        this.menu = ["home", "About", "statistics"]
    }

    onClick(name) {
        let event = new CustomEvent('onClick', {
            detail:{
                selectedPage: name,
            }
        });
        this.dispatchEvent(event);
    }

    render() {
        return html`
            
            <div class="sidebar__content">
                ${this.menu.map(name => html`
                    <p><button @click="${() => this.onClick(name)}"> ${name.toUpperCase()} </button></p>
                `)}
            </div>
            
        `;
    }

}

customElements.define('sidebar-menu', SidebarMenu);

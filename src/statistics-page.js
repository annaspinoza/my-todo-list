import { LitElement, html, css } from 'lit-element';

class StatisticsPage extends LitElement {
    static get properties() {
        return {
            listNames: { type: Array },
            data: { type: Array },
        }
    }
    static get styles() {
        return css`
            h1 {
                text-align: center;
            }

            p {
                margin-left: auto;
                margin-right: auto;
                width: 90%;
                text-align: justify;
            }

            ul {
                list-style-position: inside;
                margin-left: 50px;
                padding: 0;
            }
        `;
    }
    constructor(){
        super();
        this.listNames = [];
        this.data = [];
    }

    render() {
        return html`
            <h1>Statistics</h1>
            <p> Number of created lists: ${this.data.length} </p>   
            <p> Number of total completed items: ${this.data.reduce(( sum,dataObj) => sum + dataObj["completedItems"], 0)} </p>
            <p> Total number of items per list: 
                <ul> 
                    ${this.data.map(dataObj => html`
                        <li> ${dataObj["listName"]} : ${dataObj["totalItems"]}</li>
                    `)}
                </ul>
            </p> 
            <p> Number of competed items per list: 
                <ul> 
                    ${this.data.map(dataObj => html`
                        <li> ${dataObj["listName"]} : ${dataObj["completedItems"]}</li>
                    `)}
                </ul>
            </p>
            <p> Number of items to be completed per list: 
                <ul> 
                    ${this.data.map(dataObj => html`
                        <li> ${dataObj["listName"]} : ${dataObj["uncompleteItems"]}</li>
                    `)}
                </ul>
            </p>             
        `;
    }

    updated(changedProperties) {
        if(changedProperties.has("listNames")) {
            this.data = this.listNames.map(name => {
                let value = JSON.parse(localStorage.getItem(name)) ?? []
                let statistics = { }
                let checked = value.filter(item => item.checked === true)
                let unchecked = value.filter(item => item.checked === false)
                statistics["listName"] = name
                statistics["totalItems"] = value.length
                statistics["completedItems"] = checked.length
                statistics["uncompleteItems"] = unchecked.length
                return statistics
            });
        }
    }
}

customElements.define('statistics-page', StatisticsPage);

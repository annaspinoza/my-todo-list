import { LitElement, html, css } from 'lit-element';
import './todo-list.js';
import './sidebar-menu.js';
import './statistics-page.js';
import './about-page.js';
import './my-button.js'

class TodolistApp extends LitElement {
    static get properties() {
        return {
            lists: { type: Array },
            selectedPage: { type: String },
            selectedList: { type: String }, 
        };
    }

    static get styles(){
        return css`
            :host {
                font-family: arial, helvetica, sans-serif;
            }

            .header {
                display: flex;
                justify-content: center;
                background-color: orchid;
                padding: 16px 40px;
                margin-left: 150px;
            }

            .header__title {
                font-size: 36px;
                color: indigo;
            }

            .sidebar {
                top: 0;
                left: 0;
                position: fixed;
                height: 100%;
                float: left;
                width: 150px;
                background-color: lavenderblush;
            }

            .main-block{
                margin-left: 150px;
            }

            .container {
                display: flex;
                justify-content: center;
                padding: 0 60px 16px;
            }

            .list {
                list-style-type: none;
                margin: 0;
                padding: 0;
            }

            .list__title {
                display: flex;
                justify-content: center;
                padding: 0 60px;
                font-weight: normal;
                font-size: 24px;
            }

            .list__item {
                line-height: 1.5;
                margin-bottom: 8px;
            }

            .list__button {
                outline: none;
                background: transparent;
                border: none;
                font-size: 18px;
            }
        `;
    }
    
    constructor(){
        super();
        this.inputLabel = 'Create a new list:',
        this.inputPlaceholder = 'Insert list name',
        this.lists = [];
        this.selectedPage = 'home';
        this.selectedList = '';
        this.buttonName = 'X';
        this.buttonType = {"genericButton":true , delete:true};
    }

    connectedCallback() {
        super.connectedCallback();
        this.lists = JSON.parse(localStorage.getItem("myLists")) ?? [];
    }

    addNewList(e) {
        let newList = {
            value: e.detail.value,
        }
        this.lists = [...this.lists, newList.value];
    }
    
    onRemoveList(e) {
        let listToRemove = e.detail.value;
        localStorage.removeItem(listToRemove);
        this.lists = this.lists.filter(el => el!=listToRemove);
        this.selectedPage = 'home';
    }

    onOpenList(name){
        this.selectedList = name;
        this.selectedPage = 'selected-list';
    }

    onDeleteList(name){
        let listToRemove = name;
        localStorage.removeItem(listToRemove);
        this.lists = this.lists.filter(el => el!=listToRemove);
    }

    whatToRender(e){
        this.selectedPage = e.detail.selectedPage.toLowerCase();
    }
        
    mainBlock(){
        switch (this.selectedPage) {
            case 'about':
                return html`<about-page></about-page>`;
            case 'statistics':
                return html`<statistics-page .listNames="${this.lists}"></statistics-page>`;
            case 'home':
                return html`
                    <div class="container">
                        <input-field .label="${this.inputLabel}" .placeholder="${this.inputPlaceholder}" @onNewEntry="${this.addNewList}">ciao</input-field>
                    </div>
                    <h1 class="list__title">Existing lists:</h1>
                    <div class="container">
                        <ul class="list">
                            ${this.lists.map(name => html`
                                <li class="list__item">
                                    <button title="${name}" class="list__button" @click="${() => this.onOpenList(name)}"> ${name} </button>
                                    <my-button .buttonType='${this.buttonType}' .buttonName='${this.buttonName}' @onClick="${() => this.onDeleteList(name)}"></my-button>
                                </li>
                            `)}
                        </ul>
                    </div>
                `;
            case 'selected-list':
                return html`
                    <todo-list .listName="${this.selectedList}" @removeList="${this.onRemoveList}"></todo-list>
                `;
            default: return html`<div>...Oops something wrong happened</div>`;
        }
        
    }

    render() {
        return html`
            <header class="header" role="banner">
                <h1 class="header__title">My todo list app<h1>
            </header>

            <div class="sidebar">
                <sidebar-menu @onClick="${this.whatToRender}"></sidebar-menu>
            </div>

            <div class="main-block">
                ${this.mainBlock()}
            </div>
        `;
    }

    updated(changedProperties) {
        if(changedProperties.has("lists")) {
            localStorage.setItem("myLists", JSON.stringify(this.lists));
        }
    }
}

customElements.define('todolist-app', TodolistApp);
